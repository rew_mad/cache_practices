<?php declare(strict_types=1);

namespace Daemon\EventProcessors;

use Daemon\InvalidateRule;
use Daemon\CacheWarmers\BaseCacheWarmer;

class DeleteEventProcessor extends BaseEventProcessor
{
    public function processRule(InvalidateRule $rule, BaseCacheWarmer $warmer): void
    {
        // TODO: Implement processRule() method.
    }
}
<?php declare(strict_types=1);

namespace Daemon\EventProcessors;

use Daemon\InvalidateRule;
use Daemon\CacheWarmers\BaseCacheWarmer;
use MySQLReplication\Event\RowEvent\TableMap;

class UpdateEventProcessor extends BaseEventProcessor
{
    /**
     * @throws \Exception
     */
    public function processRule(InvalidateRule $rule, BaseCacheWarmer $warmer): void
    {
        if ($rule->isSingleKey()) {
            $this->rewarmWithSingleKey($warmer);
        } elseif ($rule->hasPrimaryKey()) {
            foreach ($this->eventInfo['values'] as $change) {
                $pk = $rule->getPrimaryKey();
                if (isset($change['before'][$pk], $change['after'][$pk])) {
                    $id = $change['after'][$pk];
                    $entity = $warmer->fetchById($id);
                    $warmer->warm($warmer->getKey($entity), $entity);

                    if ($change['before'][$pk] !== $id) {
                        $warmer->delete($warmer->getKey([$pk => $change['before'][$pk]]));
                    }
                }
            }
        }
    }
}
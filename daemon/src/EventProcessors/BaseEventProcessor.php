<?php declare(strict_types=1);

namespace Daemon\EventProcessors;

use Daemon\CacheWarmers\{
    BaseCacheWarmer,
    BookCacheWarmer,
    BookCollectionCacheWarmer
};
use Daemon\InvalidateRule;
use MySQLReplication\Event\RowEvent\TableMap;

abstract class BaseEventProcessor
{
    private const HASH_BOOK = 'cache:hash:book';
    private const HASH_BOOK_COLLECTION = 'cache:hash:book_collection';

    /** @var array */
    protected $eventInfo;
    /** @var array */
    protected $dependencies;
    /** @var BookCacheWarmer */
    protected $bookCacheWarmer;
    /** @var BookCollectionCacheWarmer */
    protected $bookCollectionCacheWarmer;

    abstract public function processRule(InvalidateRule $rule, BaseCacheWarmer $warmer): void;

    public function __construct(BookCacheWarmer $bookCacheWarmer, BookCollectionCacheWarmer $bookCollectionCacheWarmer)
    {
        $this->bookCacheWarmer = $bookCacheWarmer;
        $this->bookCollectionCacheWarmer = $bookCollectionCacheWarmer;
    }

    public function setEventInfo(array $eventInfo): self
    {
        $this->eventInfo = $eventInfo;
        return $this;
    }

    public function setDependencies(array $dependencies): self
    {
        $this->dependencies = $dependencies;
        return $this;
    }

    /**
     * @param $hashName
     * @return BaseCacheWarmer
     * @throws \Exception
     */
    public function getWarmerBy($hashName): BaseCacheWarmer
    {
        switch ($hashName) {
            case self::HASH_BOOK:
                return $this->bookCacheWarmer;
            case self::HASH_BOOK_COLLECTION:
                return $this->bookCollectionCacheWarmer;
        }
        throw new \Exception("Cannot determine warmer for {$hashName}");
    }

    protected function getRulesFromDependencies(TableMap $tableMap): array
    {
        return $this->dependencies[$tableMap->getDatabase()][$tableMap->getTable()] ?? [];
    }

    protected function rewarmWithSingleKey(BaseCacheWarmer $warmer): void
    {
        $warmer->fetchAllEntities();
        $gkey = $warmer->getKey([]);
        foreach ($warmer as $entity) {
            $warmer->warm($gkey, $entity);
        }
    }

    public function process(): void
    {
        if (!$this->eventInfo['changedRows']
            || empty($this->eventInfo['values'])
            || empty($this->eventInfo['tableMap'])
            || !\count($this->eventInfo['values'])
        ) {
            return;
        }

        /** @var TableMap $tableMap */
        $tableMap = $this->eventInfo['tableMap'];
        /** @var InvalidateRule[] $rules */
        $rules = $this->getRulesFromDependencies($tableMap);

        foreach ($rules as $rule) {
            $warmer = $this->getWarmerBy($rule->getHashName());
            
            $this->processRule($rule, $warmer);
        }
    }
}
<?php declare(strict_types=1);

namespace Daemon\EventProcessors;

use Daemon\InvalidateRule;
use Daemon\CacheWarmers\BaseCacheWarmer;
use MySQLReplication\Event\RowEvent\TableMap;

class InsertEventProcessor extends BaseEventProcessor
{
    public function processRule(InvalidateRule $rule, BaseCacheWarmer $warmer): void
    {
        if ($rule->isInsertIgnored()) {
            return;
        }

        if ($rule->hasPrimaryKey()) {
            $pk = $rule->getPrimaryKey();
            foreach ($this->eventInfo['values'] as $change) {
                $id = $change[$pk];
                $entity = $warmer->fetchById($id);
                if (!empty($entity)) {
                    $warmer->warm($warmer->getKey($entity), $entity);
                }
            }
        } elseif ($rule->isSingleKey()) {
            $this->rewarmWithSingleKey($warmer);
        }
    }
}